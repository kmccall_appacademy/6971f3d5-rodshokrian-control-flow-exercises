# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.gsub(/[a-z]/, "")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.odd? ? str.slice(str.length / 2) : str.slice((str.length / 2) - 1..(str.length / 2))
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.length - str.gsub(/[aeiou]/, "").length
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  a = 1
  (1..num).each {|i| a *= i}
  a
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each_with_index {|el, idx| idx != (arr.length - 1) ? str << (el + separator) : str << el}
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.split("").map.with_index {|x, i| i.even? ? x.downcase : x.upcase}.join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(" ").map{|word| word.length >= 5 ? word.reverse : word}.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |i|
    if i % 5 == 0 && i % 3 == 0
      r = "fizzbuzz"
    elsif i % 3 == 0
      r = "fizz"
    elsif i % 5 == 0
      r = "buzz"
    else
      r = i
    end
  arr << r
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  i = -1
  new_arr = Array.new(arr.length)
  arr.each do |el|
    new_arr[i] = el
    i -= 1
  end
  return new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  bool = TRUE
  bool = FALSE if num == 1

  (2...num).each do |i|
    bool = FALSE if num % i == 0
  end
  bool
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = Array.new
  (1..num).each do |i|
    if num % i == 0
      arr.push(i)
    end
  end
  return arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  results = []
  (1..num).each do |i|
    results << i if num % i == 0 && prime?(i) == true
  end
  results
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_int = 0
  even_int = 0
  even_count = 0
  arr.each do |i|
    if i.odd?
      odd_int = i
    else
      even_int = i
      even_count += 1
    end
  end
  even_count > 1 ? odd_int : even_int

end
